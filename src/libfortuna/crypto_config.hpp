/*
Copyright 2013, 2014, 2015, 2017, 2018 Adam Mizerski <adam@mizerski.pl>

This file is part of fortuna.

libfortuna is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libfortuna is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with libfortuna.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
This file exists, to enable mocking in tests.
*/


#ifndef FORTUNA_CRYPTO_CONFIG_HPP
#define FORTUNA_CRYPTO_CONFIG_HPP

#include <cryptopp/aes.h>
#include <cryptopp/sha3.h>


namespace fortuna {


using GeneratorAes = CryptoPP::AES;
using GeneratorKeyHash = CryptoPP::SHA3;
using PoolHash = CryptoPP::SHA3_256;


} // namespace fortuna

#endif // FORTUNA_CRYPTO_CONFIG_HPP
