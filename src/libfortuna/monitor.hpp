/*
Copyright 2013, 2014, 2015, 2017, 2018 Adam Mizerski <adam@mizerski.pl>

This file is part of fortuna.

libfortuna is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libfortuna is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with libfortuna.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
Based on Herb Sutter's monitor implementation presented on
https://channel9.msdn.com/Shows/Going+Deep/C-and-Beyond-2012-Herb-Sutter-Concurrency-and-Parallelism at 40:30,
which is, I hope, public domain.
*/

#ifndef FORTUNA_MONITOR_HPP
#define FORTUNA_MONITOR_HPP

#include <utility>
#include <shared_mutex>


namespace fortuna {


template <class T>
class monitor
{
private:
    T obj;
    mutable std::shared_timed_mutex shared_mutex;

public:
    monitor() = default;

    template <typename... Args>
    monitor(Args&&... args)
        : obj(std::forward<Args>(args)...)
    {}

    monitor(const monitor&) = delete;
    monitor& operator=(const monitor&) = delete;

    template <typename F>
    auto exec_ro(F f) const
    {
        std::shared_lock<std::shared_timed_mutex> shared_lock{shared_mutex};
        return f(obj);
    }

    template <typename F>
    auto exec_rw(F f)
    {
        std::unique_lock<std::shared_timed_mutex> unique_lock{shared_mutex};
        return f(obj);
    }
};


} // namespace fortuna

#endif // FORTUNA_MONITOR_HPP
