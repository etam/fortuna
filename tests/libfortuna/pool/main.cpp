/*
Copyright 2018 Adam Mizerski <adam@mizerski.pl>

This file is part of fortuna.

libfortuna is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libfortuna is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with libfortuna.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pool.hpp"


int main()
{
    auto pool = fortuna::Pool{};
    auto expected_output = byte{0};
    auto total_length = 0ul;

    {
        const auto data = byte{5};
        pool.add_random_event(1, &data, 1);
        expected_output += 1 + 1 + 5;
        total_length += 1;

        if (pool.get_total_length_of_appended_data() != total_length) {
            return 1;
        }
    }

    {
        const byte data[] = {2, 7};
        pool.add_random_event(1, data, 2);
        expected_output += 1 + 2 + 2 + 7;
        total_length += 2;

        if (pool.get_total_length_of_appended_data() != total_length) {
            return 2;
        }
    }

    {
        auto final = byte{0};
        pool.get_hash_and_clear(&final);

        if (final != expected_output) {
            return 3;
        }
        if (pool.get_total_length_of_appended_data() != 0) {
            return 4;
        }

        expected_output = 0;
        total_length = 0;
    }

    {
        const byte data[] = {8, 1, 4};
        pool.add_random_event(5, data, 3);
        expected_output += 5 + 3 + 8 + 1 + 4;
        total_length += 3;

        if (pool.get_total_length_of_appended_data() != total_length) {
            return 5;
        }
    }

    {
        auto final = byte{0};
        pool.get_hash_and_clear(&final);

        if (final != expected_output) {
            return 6;
        }
        if (pool.get_total_length_of_appended_data() != 0) {
            return 7;
        }
    }
}
