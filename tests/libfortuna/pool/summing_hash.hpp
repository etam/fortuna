/*
Copyright 2018 Adam Mizerski <adam@mizerski.pl>

This file is part of fortuna.

libfortuna is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libfortuna is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with libfortuna.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FORTUNA_SUMMING_HASH_HPP
#define FORTUNA_SUMMING_HASH_HPP

#include <cstddef>

typedef unsigned char byte;


class SummingHash
{
private:
    byte count = 0;

public:
    static constexpr
    const std::size_t DIGESTSIZE = 1;

    void Update(const byte* ptr, std::size_t len)
    {
        for (const auto end = ptr + len; ptr != end; ++ptr) {
            count += *ptr;
        }
    }

    void Final(byte* ptr)
    {
        *ptr = count;
        count = 0;
    }
};


#endif // FORTUNA_SUMMING_HASH_HPP
